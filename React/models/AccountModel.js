import {InvestmentModel} from "./InvestmentModel.js";

export class AccountModel{

    constructor(props){
        this.data={          
            accountBalance:0,
            type:"",
            masterName:"",
            interest:0,
            investmentList:[]
        };
        this.create(props);
    }

    get masterName(){
        return this.data.masterName;
    }

    create(props){
        Object.assign(this.data, props);
    }

    createInvestment(investmentBalance,investmentTime){
        this.data.investmentList.push(new InvestmentModel(investmentBalance,investmentTime))
    }

    getinvestmentList(){
        return this.data.investmentList;
    }

    getAccountBalance(){
        return {
            accountBalance:this.data.accountBalance,
            investmentList:this.getinvestmentList().map((investment)=>{
                return {
                    investmentBalance:investment.investmentBalance,
                    investmentTime:investment.investmentTime    
                }
            })
        };
    }

    setAccountBalance(accountBalance){
        this.data.accountBalance = accountBalance;
    }

    addMoney(money){
       this.data.accountBalance+=money;
    }

    removeMoney(money){
       this.data.accountBalance-=money;
    }


}