
export class InvestmentModel{
    constructor(investmentBalance,investmentTime){
        this.data={
            investmentBalance:investmentBalance,
            investmentTime:investmentTime        
        }
    } 
    

    get investmentBalance(){
        return this.data.investmentBalance;
    }

    get investmentTime(){
        return this.data.investmentTime;
    }


}