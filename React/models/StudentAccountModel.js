import {AccountModel} from "./AccountModel.js";

export class StudentAccountModel extends AccountModel{

    constructor(props){
        props.type = "student";
        props.interest = 3;
        super(props);
    }

}