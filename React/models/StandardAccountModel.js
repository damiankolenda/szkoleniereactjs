import {AccountModel} from "./AccountModel.js";

export class StandardAccountModel extends AccountModel{

    constructor(props){
        props.type = "standard";
        props.interest = 2.5;
        super(props);
    }

}