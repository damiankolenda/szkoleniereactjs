import {StudentAccountModel} from "../models/StudentAccountModel.js";
import {StandardAccountModel} from "../models/StandardAccountModel.js";

export class AccountFactory{
    constructor(props){
        if(props.type==="standard"){
            return new StandardAccountModel(props);
        }else if(props.type==="student"){
            return new StudentAccountModel(props);
        }else{
            return new StandardAccountModel(props);
        }
    }
}