import {AccountFactory} from "../factories/AccountFactory.js"

export class AccountRepository{

    constructor(){
        this.accountList = [];       
    }

    addAccount(props){
       this.accountList.push(new AccountFactory(props)); 
    }
    
    findAccount(masterName){
        return this.accountList.find((account)=>{
            return account.data.masterName === masterName;
        });
    }

    putMoney(money,masterName){
        let account = this.findAccount(masterName);
        account.addMoney(money);
    }

    takeMoney(money,masterName){
        let account = this.findAccount(masterName);
        account.removeMoney(money);
    }

    getAccountBalance(masterName){
        let account = this.findAccount(masterName);
        return account.getAccountBalance();
    }

    getAccountMaserNameList(){
        return this.accountList.map((account)=>{
            return account.masterName;
        });
    }

    createInvestment(investmentBalance,investmentTime,masterName){
        let account = this.findAccount(masterName);
        account.createInvestment(investmentBalance,investmentTime)
    }

    getinvestmentList(masterName){
        let account = this.findAccount(masterName);
        return account.getinvestmentList();        
    }
}