import { AccountRepository } from '../repositories/AccountRepository.js';

export class AccountService{

    constructor(){
        this.accountRepository = new AccountRepository();
    }

    createAccount(props){
        if(!this.issetAccount(props.masterName)){           
            this.accountRepository.addAccount(props);
        }else{
           console.log("Konto już isnieje"); 
        }     
    }

    issetAccount(masterName){
        if(this.accountRepository.findAccount(masterName) === undefined){           
            return false;
        }else{
           return true;
        }     
    }

    putMoneyAccount(money,masterName){
        if(this.issetAccount(masterName)){
            this.accountRepository.putMoney(money,masterName);
        }else{
           console.log(`Konto ${masterName} nie isnieje`); 
        } 
    }

    takeMoneyAccount(money,masterName){
        if(this.issetAccount(masterName)){
            this.accountRepository.takeMoney(money,masterName);
        }else{
           console.log(`Konto ${masterName} nie isnieje`); 
        }
    }

    getAccountMaserNameList(){
        return this.accountRepository.getAccountMaserNameList();
    }

    getAccountBalance(masterName){
        if(this.issetAccount(masterName)){
            return this.accountRepository.getAccountBalance(masterName);
        }else{
            console.log(`Konto ${masterName} nie isnieje`); 
            return 0;
        }
    }

    createInvestment(investmentBalance,investmentTime,masterName,isSecretStash){
        if(this.issetAccount(masterName)){
            if(!isSecretStash){
                this.takeMoneyAccount(investmentBalance,masterName);
            }
            this.accountRepository.createInvestment(investmentBalance,investmentTime,masterName);

        }else{
            console.log(`Konto ${masterName} nie isnieje`); 
            return 0;
        }
        
    }

    getinvestmentList(masterName){
        if(this.issetAccount(masterName)){
            return this.accountRepository.getinvestmentList(masterName);
        }else{
            console.log(`Konto ${masterName} nie isnieje`); 
            return [];
        }       
    }

    getAccount(masterName){
        if(this.issetAccount(masterName)){
            return this.accountRepository.findAccount(masterName).data;
        }else{
            console.log(`Konto ${masterName} nie isnieje`); 
            return {
                data:{          
                    accountBalance:0,
                    type:"",
                    masterName:"",
                    interest:0,
                    investmentList:[]
                }
            };
        }  
    }

}