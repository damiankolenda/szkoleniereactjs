import React from 'react';
import ReactDOM from 'react-dom';
import { Async } from 'react-promise';
import { BrowserRouter, Route, Link, Switch, NavLink } from 'react-router-dom';
import Main from './components/Main.jsx';

ReactDOM.render((
    <BrowserRouter>
               <Main />
    </BrowserRouter>
), document.getElementById('app'))