import React from 'react';
import ReactDOM from 'react-dom';
import { Async } from 'react-promise';
import { BrowserRouter, Route, Link, Switch, NavLink } from 'react-router-dom';
import { AccountService } from '../services/AccountService.js';
import { AccountDetail } from './AccountDetail/AccountDetailComponent.js';
import { Layout } from './Layout/LayoutComponent.js';

export default class Main extends React.Component {
    constructor(props){
        super(props);
        // Tutaj ćwiczenie realizujemy
        this.test = new AccountService();

        this.test.createAccount({
            type:"standard",
            masterName:"Anna"            
        });

        this.test.createAccount({
            type:"student",
            masterName:"Kamil"            
        });


        this.test.putMoneyAccount(10000,"Anna");
        this.test.putMoneyAccount(4000,"Kamil"); 

        this.test.createInvestment(4000,4,"Anna",false);
        this.test.createInvestment(1000,12,"Kamil",true);
        this.test.createInvestment(1000,24,"Anna",false);

        this.state = {
            account: null,
            selectMasterName: "",
            masterNameList:this.test.getAccountMaserNameList()
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            selectMasterName: event.target.value,
            account:this.test.getAccount(event.target.value)
        });
    }

    render(){
        return (
            <div>  
                
                <Layout>
                        <label>
                        Wybierz konto:
                        <select value={this.state.selectMasterName} onChange={(e)=> this.handleChange(e)}>
                                <option value="">Wybierz</option>
                            {
                            this.state.masterNameList.map((masterName,key)=>
                                <option key={key} value={masterName}>{masterName}</option>
                            )
                            }
                        </select>
                    </label>

                    <AccountDetail account={this.state.account} />
                </Layout>

            </div>          
        );
    }
}