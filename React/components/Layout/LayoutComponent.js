import React from 'react';
import {Header} from './HeaderComponent.js';
import {Footer} from './FooterComponent.js';

export class Layout extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return (
            <div>
                 <Header/>
                    {this.props.children}
                 <Footer />
            </div>
        );
    }
}