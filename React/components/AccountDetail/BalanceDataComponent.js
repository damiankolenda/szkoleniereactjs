import React from 'react';

export class BalanceData extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return(

                <div>
                    <h2>Saldo</h2>
                    <p>stan konta: {this.props.accountBalance} zł</p>                    
                </div>
        );
    }
}