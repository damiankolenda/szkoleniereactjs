import React from 'react';
import { AccountService } from '../../services/AccountService.js';
import { BrowserRouter, Route, Link, Switch, NavLink } from 'react-router-dom';
import { AccountData } from './AccountDataComponent.js';
import { BalanceData } from './BalanceDataComponent.js';
import { InvestmentData } from './InvestmentDataComponent.js';



export class AccountDetail extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return (
            <div>
                {this.props.account != null &&
                   	<div>
                        <NavLink to="" activeClassName="selected">Dane konta</NavLink>    
                        <NavLink to="balance" activeClassName="selected">Stan Konta</NavLink>  
                        <NavLink to="investment" activeClassName="selected">Lokaty</NavLink>  

                        <Switch>
                            <Route exact={true} path="/" render={() => <AccountData masterName={this.props.account.masterName} type={this.props.account.type} /> } />
                            <Route path="/balance" render={() => <BalanceData accountBalance={this.props.account.accountBalance} /> } />
                            <Route path="/investment" render={() => <InvestmentData investmentList={this.props.account.investmentList} /> } />
                        </Switch>  
                   </div>
                }  
            </div>
        );
    }
}