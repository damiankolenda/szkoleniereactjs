import React from 'react';

export class InvestmentData extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return(

                <div>
                    <h2>Lokaty</h2>

                    <ul>
                            {this.props.investmentList.map( (investment,key)=>
                                <li key={key}>
                                    kwota: {investment.investmentBalance} zł ilość miesięcy: {investment.investmentTime}
                                </li>
                            )}
                    </ul>
                </div>
        );
    }
}