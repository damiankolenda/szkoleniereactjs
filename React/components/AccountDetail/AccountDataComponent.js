import React from 'react';

export class AccountData extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return(
                <div>
                    <h2>Dane konta</h2>
                    <p>Nazwa właściciela: {this.props.masterName}</p>
                    <p>Typ konta: {this.props.type}</p>
                </div>
        );
    }
}